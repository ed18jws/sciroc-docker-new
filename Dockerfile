FROM registry.gitlab.com/competitions4/sciroc/dockers/sciroc

LABEL maintainer="Jared Swift <ed18jws@leeds.ac.uk>"

ARG REPO_WS=/ws
RUN mkdir -p $REPO_WS/src
WORKDIR /home/user/$REPO_WS

# TODO: Put inside ./ws your ROS packges
COPY ./ws /home/user/ws

# TODO: add here the debians you need to install
#RUN apt install -y ros-melodic-<pkg_name> pal-ferrum-<pkg_name> <apt-pkg>
#RUN apt-get -y update
#RUN apt install -y libasound-dev
#RUN apt install -y python-pyaudio
#RUN apt install -y swig
#RUN apt install -y festival
#RUN bash -c "apt-get -y update"
RUN bash -c "apt-get install -y libasound-dev"
RUN bash -c "apt-get install -y python-pyaudio"
RUN bash -c "apt-get install -y swig"
RUN bash -c "apt-get install -y festival"
RUN bash -c "rm /usr/lib/python2.7/dist-packages/cv2.x86_64-linux-gnu.so"
RUN bash -c "apt-get install -y curl"
RUN bash -c "curl https://bootstrap.pypa.io/pip/2.7/get-pip.py -o get-pip.py"
RUN bash -c "python get-pip.py"
#RUN bash -c "pip install -r /home/user/ws/src/software/multiple_object_tracking/src/multiple_object_tracking/ab3mdot/requirements.txt"
RUN bash -c "pip install opencv-python==4.2.0.32"
RUN bash -c "pip install torch==1.4.0 torchvision==0.5.0"
RUN bash -c "pip install pocketsphinx"
RUN bash -c "pip install future"
RUN bash -c "pip install dialogflow==1.1.1 sounddevice soundfile"
RUN bash -c "export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:/home/user/ws/src/Sciroc2EP1-SimulatedEnv/models"
#Build and source your ros packages 
RUN bash -c "source /opt/pal/ferrum/setup.bash \
    && catkin build -DCATKIN_ENABLE_TESTING=0 --cmake-args -DCMAKE_C_FLAGS='-Wno-shadow -Wno-dev' \
    && echo 'source /opt/pal/ferrum/setup.bash' >> ~/.bashrc \
    && echo 'source /home/user/ws/devel/setup.bash' >> ~/.bashrc"
ENTRYPOINT ["bash"]

